package com.hfad.bestbeautyassistant.toolbar;

import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.hfad.bestbeautyassistant.R;

/**
 * Created by McLaine on 17.05.2017.
 */

public class ToolbarForAllWindows extends AppCompatActivity {
    private Toolbar mToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);

        Drawable backIconDrawable = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp);
        backIconDrawable.setColorFilter(ContextCompat.getColor(this, R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        mToolbar.setNavigationIcon(backIconDrawable);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //TODO
            }
        });
    }

    final static int MENU_ITEM_YELLOW_STAR = 111;
    final static int MENU_ITEM_SETTINGS = 112;
    final static int MENU_ITEM_APP_EVALUTION = 113;
    final static int MENU_ITEM_EXIT = 114;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem itemYelowStar = menu.add(0, MENU_ITEM_YELLOW_STAR, 0, "YELLOW_STAR");
        Drawable drawableIconYelowStar = ContextCompat.getDrawable(this, R.drawable.ic_star_yellow);
        itemYelowStar.setIcon(drawableIconYelowStar);
        itemYelowStar.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem itemAppEvaluation = menu.add(1, MENU_ITEM_APP_EVALUTION, 1, "Оценить приложение");
        itemAppEvaluation.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        MenuItem itemSettings = menu.add(2, MENU_ITEM_SETTINGS, 2, "Настройки");
        itemSettings.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        MenuItem itemExit = menu.add(2, MENU_ITEM_EXIT, 2, "Выход");
        itemExit.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        return super.onCreateOptionsMenu(menu);
    }
}
