package com.hfad.bestbeautyassistant.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;
import com.hfad.bestbeautyassistant.R;
import com.hfad.bestbeautyassistant.adapters.ContentRecyclerAdapter;
import com.hfad.bestbeautyassistant.db.DbHelper;
import com.hfad.bestbeautyassistant.model.Categories;
import com.hfad.bestbeautyassistant.model.Content;
import com.hfad.bestbeautyassistant.toolsAndConstants.DbConstant;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity implements ContentRecyclerAdapter.OnContentClickItem{
    final static int MENU_ITEM_YELLOW_STAR  = 111;
    final static int MENU_ITEM_SETTINGS  = 112;
    final static int MENU_ITEM_APP_EVALUTION  = 113;
    final static int MENU_ITEM_EXIT  = 114;
    public static final String KEY_CATEGORIES_ID = "KEY_CATEGORIES_ID";
    private Toolbar mToolbar;
    RecyclerView m_RecyclerView;
    ContentRecyclerAdapter m_ContentRecyclerAdapter;
    private ArrayList<Content> arrContent = new ArrayList<>();


       @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list);

        m_RecyclerView = (RecyclerView) findViewById(R.id.recyclerViewListActivity);

        long nId = -1;
        String strName = "";
        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                Object o = bundle.get(KEY_CATEGORIES_ID);
                if (o instanceof Categories){
                    Categories categories = (Categories) o;
                    nId = categories.getId() * -1;
                    strName = categories.getName();
                } else if (o instanceof Content){
                    Content content = (Content) o;
                    nId = content.getId();
                    strName = content.getName();
                }

            }
        }

        if (strName.isEmpty()){
            Toast.makeText(this, "Name is Empty", Toast.LENGTH_SHORT).show();
            finish();
        }


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false); //false значит что нам не нужно переворачивать список
        m_RecyclerView.setLayoutManager(linearLayoutManager);

        arrContent = getParentById(nId);
        m_ContentRecyclerAdapter = new ContentRecyclerAdapter (this ,arrContent);
        m_ContentRecyclerAdapter.setOnContentClickItemListener(this);
        m_RecyclerView.setAdapter(m_ContentRecyclerAdapter);

        mToolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(strName);

        Drawable backIconDrawable = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp);
        backIconDrawable.setColorFilter(ContextCompat.getColor(this, R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        mToolbar.setNavigationIcon(backIconDrawable);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem itemYelowStar = menu.add(0,MENU_ITEM_YELLOW_STAR, 0, "YELLOW_STAR");
        Drawable drawableIconYelowStar = ContextCompat.getDrawable(this, R.drawable.ic_star_yellow);
        itemYelowStar.setIcon(drawableIconYelowStar);
        itemYelowStar.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem itemAppEvaluation  = menu.add(1,MENU_ITEM_APP_EVALUTION, 1, "Оценить приложение");
        itemAppEvaluation.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        MenuItem itemSettings = menu.add(2,MENU_ITEM_SETTINGS, 2, "Настройки");
        itemSettings.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        MenuItem itemExit = menu.add(2,MENU_ITEM_EXIT, 2, "Выход");
        itemExit.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        return super.onCreateOptionsMenu(menu);
    }

    public ArrayList<Content> getParentById(long nId){
        ArrayList<Content> arrResult = new ArrayList<>();
        DbHelper dbHelper = new DbHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();
        String strRequest = DbConstant.TableContent.Fields.PARENT + "=?";
        String arrArgs[] = new String[]{Long.toString(nId)};
        Cursor cursor = db.query(DbConstant.TableContent.TABLE_NAME, null, strRequest, arrArgs, null, null, null);
        try {
            if (cursor != null && cursor.moveToFirst()) {
                do {
                    arrResult.add(new Content(cursor));
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
            db.close();
        }
        return arrResult;
    }

    @Override
    public void onContentClickItem(Content content) {
        Log.d("kan", "content->"+content.getName());
        Intent intent = null;
        if (content.getType()==DbHelper.TYPE_LIST){
            intent = new Intent(this, ListActivity.class);
        }else if (content.getType()==DbHelper.TYPE_FINISHED_ARTICLE){
            intent = new Intent(this, ArticleViewActivity.class);
        }

        if (intent!=null){
            intent.putExtra(KEY_CATEGORIES_ID, content);
            startActivity(intent);
        }

    }
}
