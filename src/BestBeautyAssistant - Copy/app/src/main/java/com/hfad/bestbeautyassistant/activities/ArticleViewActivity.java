package com.hfad.bestbeautyassistant.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.widget.Toast;

import com.hfad.bestbeautyassistant.R;
import com.hfad.bestbeautyassistant.model.Article;
import com.hfad.bestbeautyassistant.model.Content;

/**
 * Created by McLaine on 31.05.2017.
 */

public class ArticleViewActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_article_view);

        long nId = -1;
        String strName = "";
        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                Object o = bundle.get(ListActivity.KEY_CATEGORIES_ID);
                if (o instanceof Content){
                    Content content = (Content) o;
                    nId = content.getId();
                    strName = content.getName();
                }

            }
        }

        if (strName.isEmpty()){
            Toast.makeText(this, "Name is Empty", Toast.LENGTH_SHORT).show();
            finish();
        }

        String strContent = getArticleParentById(nId).getText();

        WebView webView = (WebView) findViewById(R.id.webViewArticleActivity);
        webView.loadData(strContent, "text/html", null);
    }

    private Article getArticleParentById(long nId){
//        Article article = null;
//        DbHelper dbHelper = new DbHelper(this);
//        SQLiteDatabase db = dbHelper.getReadableDatabase();
//        String strRequest = DbConstant.TableArticles.Fields.PARENT + "=?";
//        String arrArgs[] = new String[]{Long.toString(nId)};
//        Cursor cursor = db.query(DbConstant.TableArticles.TABLE_NAME, null, strRequest, arrArgs, null, null, null);
//
//        try {
//            if (cursor!=null&&cursor.moveToFirst()){
//                article = new Article(cursor);
//            }
//        }finally {
//            if (cursor!=null){
//                cursor.close();
//            }
//            db.close();
//
//        }
        return new Article(-1,"Hello");
    }
    }

