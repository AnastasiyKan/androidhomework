package com.hfad.bestbeautyassistant.customView;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.TextView;

import com.hfad.bestbeautyassistant.toolsAndConstants.DbConstant;

/**
 * Created by McLaine on 17.05.2017.
 */

public class CategorieView extends TextView{
    public CategorieView(Context context, String text, String iconName, int defStyleAttr) {
        super(context, null, defStyleAttr);
        setText(text);
        int  nId= context.getResources().getIdentifier(iconName, "drawable", context.getPackageName());
        setCompoundDrawablesWithIntrinsicBounds(0,nId,0,0);
    }

}
