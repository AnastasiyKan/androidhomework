package com.hfad.bestbeautyassistant.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hfad.bestbeautyassistant.R;
import com.hfad.bestbeautyassistant.model.Content;

import java.util.ArrayList;

/**
 * Created by McLaine on 23.05.2017.
 */

public class ContentRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    private static final int CONTENT = 1011;
    private ArrayList<Content> m_arrContent;
    Context context;
    OnContentClickItem listener = null;
   //TODO перенести


    public ContentRecyclerAdapter(Context context, ArrayList<Content> arrContent){
        this.context = context;
        m_arrContent = arrContent;
    }

    public void setOnContentClickItemListener(OnContentClickItem listener) {
        this.listener = listener;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null; // View  которую возвращаем
        switch (viewType){
            case CONTENT:
                View viewContent = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_content, parent, false);
                viewHolder = new ContentViewHolder(viewContent);
                break;
        }
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)){//узнаем тип
            case CONTENT:
                ContentViewHolder contentViewHolder = (ContentViewHolder) holder;//теперь у него появились наши поля!
                final Content content = m_arrContent.get(position);
                contentViewHolder.textView.setText(content.getName());
                int  nId= context.getResources().getIdentifier(content.getIconName(), "drawable", context.getPackageName());
                contentViewHolder.imageView.setImageResource(nId);
                contentViewHolder.rootView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (listener!=null){
                            listener.onContentClickItem(content);
                        }
                    }
                });
        }
    }

    @Override
    public int getItemViewType(int position) { //возращает int который соответствует типу View
        return CONTENT;
    }

    @Override
    public int getItemCount() {
        return m_arrContent.size();
    }


    public class ContentViewHolder extends RecyclerView.ViewHolder{
        View rootView;
        ImageView imageView;
        TextView textView;

        public ContentViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            imageView = (ImageView) rootView.findViewById(R.id.imageViewItem);
            textView = (TextView) rootView.findViewById(R.id.textViewItem);

        }
    }

    public interface OnContentClickItem{
        void onContentClickItem(Content content);

    }
}
