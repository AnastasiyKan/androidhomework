package com.hfad.bestbeautyassistant.activities;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.hfad.bestbeautyassistant.R;
import com.hfad.bestbeautyassistant.customView.CategorieView;
import com.hfad.bestbeautyassistant.db.DbHelper;
import com.hfad.bestbeautyassistant.model.Categories;
import com.hfad.bestbeautyassistant.toolsAndConstants.DbConstant;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    final static int MENU_ITEM_YELLOW_STAR  = 111;
    final static int MENU_ITEM_SETTINGS  = 112;
    final static int MENU_ITEM_APP_EVALUTION  = 113;
    final static int MENU_ITEM_EXIT  = 114;
    private Toolbar mToolbar;
    View hair_activity_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mToolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        Drawable menuRightIconDrawable = ContextCompat.getDrawable(this, R.drawable.ic_more_vert_black_24dp);
        menuRightIconDrawable.setColorFilter(ContextCompat.getColor(this, R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);

        mToolbar.setOverflowIcon(menuRightIconDrawable);

        LinearLayout container= (LinearLayout) findViewById(R.id.activity_main);
        DbHelper dbHelper = new DbHelper(this);
        SQLiteDatabase db = dbHelper.getReadableDatabase();

        Cursor cursor = db.query(DbConstant.TableCategories.TABLE_NAME, null, null, null,null, null, DbConstant.TableCategories.Fields.ORDER + " ASC" );
        if (cursor!=null){
            if (cursor.moveToFirst()){
                LinearLayout.LayoutParams elementParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT, 1);
                int margin = (int) (5 * getResources().getDisplayMetrics().density);
                int padding = (int) getResources().getDimension(R.dimen.padding_element_categories);

                elementParams.setMargins(margin, margin, margin, margin);
                LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                LinearLayout linearLayout = null;

                do {
                    Categories categories = new Categories(cursor);

                    CategorieView categorieView= new CategorieView(this, categories.getName(), categories.getIconName(), R.style.Activity_main);
                    categorieView.setTextColor(getResources().getColor(R.color.colorTextActivityMain));
                    categorieView.setBackgroundResource(R.drawable.activity_main_button_bg);
                    categorieView.setPadding(padding, padding, padding, padding);
                    categorieView.setTextSize(TypedValue.COMPLEX_UNIT_PX, getResources().getDimension(R.dimen.text_size_element_categories));
                    categorieView.setGravity(Gravity.CENTER);
                    categorieView.setTag(categories);
                    categorieView.setOnClickListener(this);
                    categorieView.setLayoutParams(elementParams);
                    if (linearLayout == null || linearLayout.getChildCount()>1){
                        linearLayout = new LinearLayout(this);
                        linearLayout.setLayoutParams(layoutParams);
                        linearLayout.setOrientation(LinearLayout.HORIZONTAL);
                        container.addView(linearLayout);
                    }
                    linearLayout.addView(categorieView);

                }while (cursor.moveToNext());
            }
            cursor.close();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem itemYelowStar = menu.add(0,MENU_ITEM_YELLOW_STAR, 0, "YELLOW_STAR");
        Drawable drawableIconYelowStar = ContextCompat.getDrawable(this, R.drawable.ic_star_yellow);
        itemYelowStar.setIcon(drawableIconYelowStar);
        itemYelowStar.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem itemAppEvaluation  = menu.add(1,MENU_ITEM_APP_EVALUTION, 1, "Оценить приложение");
        itemAppEvaluation.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        MenuItem itemSettings = menu.add(2,MENU_ITEM_SETTINGS, 2, "Настройки");
        itemSettings.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(this, ListActivity.class);
        Categories categories = (Categories) v.getTag();
        intent.putExtra(ListActivity.KEY_CATEGORIES_ID, categories);
        startActivity(intent);

    }
}
