package com.hfad.bestbeautyassistant.model.engines;

import android.content.Context;

/**
 * Created by McLaine on 25.05.2017.
 */

public abstract class BaseEngine{
    private Context m_Context;

    public BaseEngine(Context m_Context) {
        this.m_Context = m_Context;
    }

    public Context getContext() {
        return m_Context;
    }
}
