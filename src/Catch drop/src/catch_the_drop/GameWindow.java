package catch_the_drop;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;

public class GameWindow extends JFrame {
    private static GameWindow game_windw;
    private static Image background;
    private static Image game_over;
    private static Image drop;
    private static float drop_left = 200;
    private static float drop_top = -100;
    private static float drop_vear = 100;
    private static long last_frame_time;
    private static int score;

    public static void main(String[] args) throws IOException {
        background = ImageIO.read(GameWindow.class.getResourceAsStream("background.png"));
        drop = ImageIO.read(GameWindow.class.getResourceAsStream("drop.png"));
        game_over = ImageIO.read(GameWindow.class.getResourceAsStream("game_over.png"));
        game_windw = new GameWindow();
        game_windw.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        game_windw.setLocation(200, 100);
        game_windw.setSize(906,478);
        game_windw.setResizable(false);
        last_frame_time = System.nanoTime();
        GameFeild game_feild = new GameFeild();
        game_feild.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                int x = e.getX();
                int y = e.getY();
                float drop_right = drop_left+drop.getWidth(null);
                float drop_bottom = drop_top+drop.getHeight(null);
                boolean is_drop = x>=drop_left && x<=drop_right && y>=drop_top&& y<=drop_bottom;
                if (is_drop){
                    drop_top=-100;
                    drop_left = (int)(Math.random()*(game_feild.getWidth()-drop.getWidth(null)));
                    drop_vear = drop_vear+10;
                    score++;
                    game_windw.setTitle("Score: " +score);
                }
            }
        });
        game_windw.add(game_feild);
        game_windw.setVisible(true);

	// write your code here
    }
    private static void onRepaint (Graphics g){
        long current_time = System.nanoTime();
        float delta_time = (current_time - last_frame_time ) * 0.000000001f;
        last_frame_time = current_time;
        drop_top = drop_top+drop_vear*delta_time;

        g.drawImage(background, 0, 0, null);
        g.drawImage(drop, (int) drop_left, (int) drop_top, (null));
        if(drop_top>game_windw.getHeight()) g.drawImage(game_over, 280, 120, null);

    }
    private static class GameFeild extends JPanel {
        @Override
        protected void paintComponent(Graphics g) {
            super.paintComponent(g);
            onRepaint(g);
            repaint();
        }

    }
}

