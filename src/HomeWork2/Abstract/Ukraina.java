package HomeWork2.Abstract;

/**
 * Created by McLaine on 11.03.2017.
 */
public class Ukraina extends Country{
    @Override
    public String nameCountry() {
        return "Ukraina";
    }

    @Override
    public String language() {
        return "ukrainian";
    }

    @Override
    public int population() {
        return 42512970;
    }

    @Override
    public int sumCity() {
        return 460;
    }

    @Override
    public int sumVVP() {
        return 131805;

    }

}
