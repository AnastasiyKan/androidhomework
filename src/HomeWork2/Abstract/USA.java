package HomeWork2.Abstract;

/**
 * Created by McLaine on 11.03.2017.
 */
public class USA extends Country {
    @Override
    public String nameCountry() {
        return "USA";
    }

    @Override
    public String language() {
        return "english";
    }

    @Override
    public int population() {
        return 318900000;
    }

    @Override
    public int sumCity() {
        return 30000;
    }

    @Override
    public int sumVVP() {
        return 17419000;
    }
}
