package HomeWork2.Abstract;

import java.util.Scanner;

/**
 * Created by McLaine on 11.03.2017.
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите одну из перечисленных стран (Китай, США, Украина): ");
        String nameCountry = scanner.nextLine();
        System.out.println(nameCountry);

        if (nameCountry.equals("Украина")) {
            Ukraina ukraina = new Ukraina();
            System.out.println("Country: " + ukraina.nameCountry());
            System.out.println("Language: " + ukraina.language());
            System.out.println("Population: " + ukraina.population());
            System.out.println("Count cities: " + ukraina.sumCity());
            System.out.println("VVP $ millions: " + ukraina.sumVVP());
        }else if (nameCountry.equals("США")) {
            USA usa = new USA();
            System.out.println();
            System.out.println("Country: " + usa.nameCountry());
            System.out.println("Language: " + usa.language());
            System.out.println("Population: " + usa.population());
            System.out.println("Count cities: " + usa.sumCity());
            System.out.println("VVP $ millions: " + usa.sumVVP());
        }else if (nameCountry.equals("Китай")) {
            China china = new China();
            System.out.println();
            System.out.println("Country: " + china.nameCountry());
            System.out.println("Language: " + china.language());
            System.out.println("Population: " + china.population());
            System.out.println("Count cities: " + china.sumCity());
            System.out.println("VVP $ millions: " + china.sumVVP());
        }else {
            System.out.println("Вы не правельно ввели страну. Введите одну из перечисленных стран (Китай, США, Украина): ");
            nameCountry = scanner.nextLine();
        }






    }
}
