package HomeWork2.Abstract;

/**
 * Created by McLaine on 11.03.2017.
 */
public class China extends Country {
    @Override
    public String nameCountry() {
        return "China";
    }

    @Override
    public String language() {
        return "chinese";
    }

    @Override
    public int population() {
        return 1357000000;
    }

    @Override
    public int sumCity() {
        return 1602;
    }

    @Override
    public int sumVVP() {
        return 10354832;
    }
}
