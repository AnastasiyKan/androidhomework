package HomeWork2.Abstract;

/**
 * Created by McLaine on 11.03.2017.
 */
public abstract class Country {
    public abstract String nameCountry();
    public abstract String language();
    public abstract int population();
    public abstract int sumCity();
    public abstract int sumVVP();
}
