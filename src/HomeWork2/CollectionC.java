package HomeWork2;

import java.util.*;

/**
 * Created by McLaine on 09.03.2017.
 */
public class CollectionC {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList =  new ArrayList<Integer>();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Вводите числа в произвольном порядке, если вы введете отрицательное число программа завершится: ");

        int i = 0;
        while (i>=0){
            i=scanner.nextInt();
            arrayList.add(i);
        }
        arrayList.remove(arrayList.size()-1);

        HashSet<Integer> hashSet = new HashSet<Integer>(arrayList);
        TreeSet<Integer>treeSet = new TreeSet<Integer>(hashSet);

        System.out.print("Отсортированный массив: "+treeSet);
        int summ = 0;
        for (Integer in : treeSet)
            summ+=in;
        double halfOf=(double) summ/treeSet.size();
        System.out.println();
        System.out.println("Средняя арифметическая: "+halfOf);

        LinkedHashSet <Integer> linkedHashSet = new LinkedHashSet<Integer>(arrayList);
        System.out.print("Числа в порядке их ввода, которые не меньше средней арифметической: ");

        for (Integer in1 : linkedHashSet)
            if (in1>=halfOf){
                System.out.print(in1+" ");

            }
        }

    }

