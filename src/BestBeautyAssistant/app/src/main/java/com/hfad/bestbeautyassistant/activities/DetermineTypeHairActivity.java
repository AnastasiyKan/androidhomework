package com.hfad.bestbeautyassistant.activities;

import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.Spinner;

import com.hfad.bestbeautyassistant.R;

public class DetermineTypeHairActivity extends AppCompatActivity {
    private Toolbar m_Toolbar;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hair_types_hair_determine_type_hair);
        m_Toolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(m_Toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {//добавляем избранные и менюшку
        getMenuInflater().inflate(R.menu.menu_main_gray_star, menu);
//
//        MenuItem item = menu.findItem(R.id.spinner);
//        Spinner spinner = (Spinner) MenuItemCompat.getActionView(item);
//
//        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.spinner_list_item_array, android.R.layout.simple_spinner_item);
//        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        spinner.setAdapter(adapter);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {// обрабатываем нажатие на кнопку Home
        int id = item.getItemId();
        if (id == android.R.id.home){
            finish();
            return true;
        }
        return true;
    }

}
