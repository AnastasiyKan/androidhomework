package com.hfad.bestbeautyassistant.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.View;

import com.hfad.bestbeautyassistant.model.Categories;
import com.hfad.bestbeautyassistant.model.Content;
import com.hfad.bestbeautyassistant.toolsAndConstants.DbConstant;

/**
 * Created by McLaine on 17.05.2017.
 */

public class DbHelper extends SQLiteOpenHelper {
    public static final int TYPE_LIST = 1;
    public static final int TYPE_FINISHED_ARTICLE = 0;
    public DbHelper(Context context) {
        super(context, DbConstant.DB_NAME, null,DbConstant.DB_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DbConstant.TableCategories.TABLE_NAME +
                " (" + DbConstant.TableCategories.Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DbConstant.TableCategories.Fields.ORDER + " INTEGER, "
                + DbConstant.TableCategories.Fields.NAME + " TEXT, "
                + DbConstant.TableCategories.Fields.ICON_NAME + " TEXT);");

        db.execSQL("CREATE TABLE " + DbConstant.TableContent.TABLE_NAME +  " ("
                + DbConstant.TableContent.Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DbConstant.TableContent.Fields.TYPE + " INTEGER, "
                + DbConstant.TableContent.Fields.PARENT + " INTEGER, "
                + DbConstant.TableContent.Fields.NAME + " TEXT, "
                + DbConstant.TableContent.Fields.ICON_NAME + " TEXT);");

        db.execSQL("CREATE TABLE " + DbConstant.TableArticles.TABLE_NAME + " ("
                + DbConstant.TableArticles.Fields.ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                + DbConstant.TableArticles.Fields.PARENT + " INTEGER, "
                + DbConstant.TableArticles.Fields.TEXT + " TEXT);");

        initData(db);
    }
    private void initData(SQLiteDatabase db){
        Categories categories = new Categories(1, "Уход за лицом", "activity_main_face");
        long nIdMenuOrder1 = db.insert(DbConstant.TableCategories.TABLE_NAME, null, categories.getContentValues());
        categories = new Categories(2, "Уход за волосами", "activity_main_hair");
        long nIdMenuOrder2 = db.insert(DbConstant.TableCategories.TABLE_NAME, null, categories.getContentValues());
        categories = new Categories(3, "Уход за руками и ногами", "activity_main_hand_feet");
        long nIdMenuOrder3 = db.insert(DbConstant.TableCategories.TABLE_NAME, null, categories.getContentValues());
        categories = new Categories(4, "Правильное питание", "activity_main_proper_nutrition");
        long nIdMenuOrder4 = db.insert(DbConstant.TableCategories.TABLE_NAME, null, categories.getContentValues());
        categories = new Categories(5, "Фитнес", "activity_main_fitness");
        long nIdMenuOrder5 = db.insert(DbConstant.TableCategories.TABLE_NAME, null, categories.getContentValues());
        categories = new Categories(6, "Магазины на карте", "activity_main_shop");
        long nIdMenuOrder6 = db.insert(DbConstant.TableCategories.TABLE_NAME, null, categories.getContentValues());


        Content content = new Content(TYPE_LIST, (-1*nIdMenuOrder2), "Прически", "activity_hair_hairstyles");
        long nIdHairs = db.insert(DbConstant.TableContent.TABLE_NAME, null, content.getContentValues());
        content = new Content(TYPE_FINISHED_ARTICLE, (-1*nIdMenuOrder2), "Типы волос", "activity_hair_types_hair");
        long nIdHairsTypes = db.insert(DbConstant.TableContent.TABLE_NAME, null, content.getContentValues());
        content = new Content(TYPE_LIST, (-1*nIdMenuOrder2), "Уход за волосами в домашних условиях", "activity_hair_hair_care");
        long nIdHairsCare = db.insert(DbConstant.TableContent.TABLE_NAME, null, content.getContentValues());
        content = new Content(TYPE_LIST, (-1*nIdMenuOrder2), "Маски для волос в домашних условиях", "activity_hair_hair_masks");
        long nIdHairsMasks = db.insert(DbConstant.TableContent.TABLE_NAME, null, content.getContentValues());
        content = new Content(TYPE_FINISHED_ARTICLE, (-1*nIdMenuOrder2), "Рецепты для экспресс-восстановления волос", "activity_hair_recipes_for_hair_restoration");
        long nIdHairsRestoration = db.insert(DbConstant.TableContent.TABLE_NAME, null, content.getContentValues());
        content = new Content(TYPE_FINISHED_ARTICLE, (-1*nIdMenuOrder2),  "Трендовые окрашивания", "activity_hair_trend_coloring");
        long nIdHairsTrendColoring = db.insert(DbConstant.TableContent.TABLE_NAME, null, content.getContentValues());

        content = new Content(TYPE_FINISHED_ARTICLE, nIdHairs, "Стильный пучок в офис и на вечеринку", "activity_hair_hairstyles_stylish_bundle");
        long nIdHairsStylishBundle = db.insert(DbConstant.TableContent.TABLE_NAME, null, content.getContentValues());
        content = new Content(TYPE_FINISHED_ARTICLE, nIdHairs, "Легкая прическа с двумя косами на каждый день", "activity_hair_hairstyles_stylish_with_two_braids");
        long nIdHairsStylishWithTwoBraids = db.insert(DbConstant.TableContent.TABLE_NAME, null, content.getContentValues());




    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }
}
