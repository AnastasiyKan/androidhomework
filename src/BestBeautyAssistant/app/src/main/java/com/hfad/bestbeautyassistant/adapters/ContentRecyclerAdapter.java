package com.hfad.bestbeautyassistant.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.hfad.bestbeautyassistant.R;
import com.hfad.bestbeautyassistant.model.Content;

import java.util.ArrayList;

/**
 * Created by McLaine on 23.05.2017.
 */

public class ContentRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>{
    ArrayList<Content> m_arrContent;
    private static final int CONTENT = 1011;


    public ContentRecyclerAdapter(ArrayList<Content> m_arrContent) { //получили список
        this.m_arrContent = m_arrContent;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        RecyclerView.ViewHolder viewHolder = null; // View  которую возвращаем
        switch (viewType){
            case CONTENT:
                View viewContent = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_content, parent, false);
                viewHolder = new ContentViewHolder(viewContent);
                break;
        }
        return viewHolder;
    }
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (getItemViewType(position)){//узнаем тип
            case CONTENT:
                ContentViewHolder contentViewHolder = (ContentViewHolder) holder;//теперь у него появились наши поля!
                Content content = m_arrContent.get(position);
                contentViewHolder.textView.setText(content.getName());
                contentViewHolder.imageView.setImageResource(Integer.parseInt(content.getIconName()));

        }

    }

    @Override
    public int getItemViewType(int position) { //возращает int который соответствует типу View
        return CONTENT;
    }

    @Override
    public int getItemCount() {
        return m_arrContent.size();
    }
    public class ContentViewHolder extends RecyclerView.ViewHolder{
        View rootView;
        ImageView imageView;
        TextView textView;

        public ContentViewHolder(View itemView) {
            super(itemView);
            rootView = itemView;
            imageView = (ImageView) rootView.findViewById(R.id.imageViewItem);
            textView = (TextView) rootView.findViewById(R.id.textViewItem);

        }
    }
}
