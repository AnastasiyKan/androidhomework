package com.hfad.bestbeautyassistant.model;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;

import com.hfad.bestbeautyassistant.toolsAndConstants.DbConstant;

/**
 * Created by McLaine on 22.05.2017.
 */

public abstract class BaseEntity {

    private long m_Id = -1; // id класса модели

    public long getId() {
        return m_Id;
    }

    public void setId(long m_Id) {
        this.m_Id = m_Id;
    }

    public abstract ContentValues getContentValues();

}
