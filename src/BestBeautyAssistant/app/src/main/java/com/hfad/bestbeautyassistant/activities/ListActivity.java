package com.hfad.bestbeautyassistant.activities;

import android.content.Intent;
import android.database.sqlite.SQLiteDatabase;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.hfad.bestbeautyassistant.R;
import com.hfad.bestbeautyassistant.adapters.ContentRecyclerAdapter;
import com.hfad.bestbeautyassistant.db.DbHelper;
import com.hfad.bestbeautyassistant.model.Categories;
import com.hfad.bestbeautyassistant.model.Content;

import java.util.ArrayList;

public class ListActivity extends AppCompatActivity implements View.OnClickListener{
    public static final String KEY_CATEGORIES_ID = "KEY_CATEGORIES_ID";
    private Toolbar mToolbar;
    RecyclerView m_RecyclerView;
    ContentRecyclerAdapter m_ContentRecyclerAdapter;
    ArrayList<Content> arrContent = new ArrayList<>();


    //    View activity_hair_hairstyles, activity_hair_types_hair, activity_hair_hair_care, activity_face_hair_masks, activity_hair_recipes_for_hair_restoration, activity_hair_trend_coloring;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hair);



        long nId = -1;
        String strName = "";
        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                Object o = bundle.get(KEY_CATEGORIES_ID);
                if (o instanceof Categories){
                    Categories categories = (Categories) o;
                    nId = categories.getId();
                    strName = categories.getName();
                }
                m_RecyclerView = (RecyclerView) findViewById(R.id.recyclerViewListActivity);
            }
        }

        if (strName.isEmpty()){
            Toast.makeText(this, "Name is Empty", Toast.LENGTH_SHORT).show();
            finish();
        }
//
//        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false); //false значит что нам не нужно переворачивать список
//        m_RecyclerView.setLayoutManager(linearLayoutManager);
//        m_ContentRecyclerAdapter = new ContentRecyclerAdapter (arrContent);
//        m_RecyclerView.setAdapter(m_ContentRecyclerAdapter);

        //TODO как работают списки, RecicleView: добавить RecicleView на активити.
        //TODO model все парсибл



//        activity_hair_hairstyles = findViewById(R.id.activity_hair_hairstyles);
//        activity_hair_hairstyles.setOnClickListener(this);
//        activity_hair_types_hair = findViewById(R.id.activity_hair_types_hair);
//        activity_hair_types_hair.setOnClickListener(this);
//        activity_hair_hair_care = findViewById(R.id.activity_hair_hair_care);
//        activity_hair_hair_care.setOnClickListener(this);
//        activity_face_hair_masks = findViewById(R.id.activity_hair_hair_masks);
//        activity_face_hair_masks.setOnClickListener(this);
//        activity_hair_recipes_for_hair_restoration = findViewById(R.id.activity_hair_recipes_for_hair_restoration);
//        activity_hair_recipes_for_hair_restoration.setOnClickListener(this);
//        activity_hair_trend_coloring = findViewById(R.id.activity_hair_trend_coloring);
//        activity_hair_trend_coloring.setOnClickListener(this);

        mToolbar= (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setTitle(strName);

        Drawable backIconDrawable = ContextCompat.getDrawable(this, R.drawable.ic_arrow_back_black_24dp);
        backIconDrawable.setColorFilter(ContextCompat.getColor(this, R.color.colorWhite), PorterDuff.Mode.SRC_ATOP);
        mToolbar.setNavigationIcon(backIconDrawable);
        mToolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }
    final static int MENU_ITEM_YELLOW_STAR  = 111;
    final static int MENU_ITEM_SETTINGS  = 112;
    final static int MENU_ITEM_APP_EVALUTION  = 113;
    final static int MENU_ITEM_EXIT  = 114;
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuItem itemYelowStar = menu.add(0,MENU_ITEM_YELLOW_STAR, 0, "YELLOW_STAR");
        Drawable drawableIconYelowStar = ContextCompat.getDrawable(this, R.drawable.ic_star_yellow);
        itemYelowStar.setIcon(drawableIconYelowStar);
        itemYelowStar.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);

        MenuItem itemAppEvaluation  = menu.add(1,MENU_ITEM_APP_EVALUTION, 1, "Оценить приложение");
        itemAppEvaluation.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        MenuItem itemSettings = menu.add(2,MENU_ITEM_SETTINGS, 2, "Настройки");
        itemSettings.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        MenuItem itemExit = menu.add(2,MENU_ITEM_EXIT, 2, "Выход");
        itemExit.setShowAsAction(MenuItem.SHOW_AS_ACTION_NEVER);

        return super.onCreateOptionsMenu(menu);
    }



    @Override
    public void onClick(View v) {
//        switch (v.getId()){
//            case R.id.activity_hair_hairstyles:
//                Intent intent = new Intent(this, HairHairstylesActivity.class);
//                startActivity(intent);
//                break;
//            case R.id.activity_hair_types_hair:
//                Intent intent1 = new Intent(this, DetermineTypeHairActivity.class);
//                startActivity(intent1);
//                break;
//            case R.id.activity_hair_hair_care:
//                Intent intent2 = new Intent(this, HairHairstylesActivity.class);
//                startActivity(intent2);
//                break;
//            case R.id.activity_hair_hair_masks:
//                Intent intent3 = new Intent(this, HairHairstylesActivity.class);
//                startActivity(intent3);
//                break;
//            case R.id.activity_hair_recipes_for_hair_restoration:
//                Intent intent4 = new Intent(this, HairHairstylesActivity.class);
//                startActivity(intent4);
//                break;
//            case R.id.activity_hair_trend_coloring:
//                Intent intent5 = new Intent(this, HairHairstylesActivity.class);
//                startActivity(intent5);
//                break;
//        }
    }
}
