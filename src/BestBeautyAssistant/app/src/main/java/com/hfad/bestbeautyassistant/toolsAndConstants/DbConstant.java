package com.hfad.bestbeautyassistant.toolsAndConstants;

/**
 * Created by McLaine on 17.05.2017.
 */

public class DbConstant {
    public static final String DB_NAME = "beauty_db";
    public static final int DB_VERSION = 1;

    public static class TableCategories {
        public static final String TABLE_NAME = "_Categories";

        public static class Fields {
            public static final String ID = "_id";
            public static final String ORDER = "_order";
            public static final String NAME = "_name";
            public static final String ICON_NAME = "_icon_name";
        }
    }

    public static class TableContent {
        public static final String TABLE_NAME = "_Content";

        public static class Fields {
            public static final String ID = "_id";
            public static final String TYPE = "_type";
            public static final String PARENT = "_parent";
            public static final String NAME = "_name";
            public static final String ICON_NAME = "_icon_name";
        }
    }

    public static class TableArticles {
        public static final String TABLE_NAME = "_Articles";

        public static class Fields {
            public static final String ID = "_id";
            public static final String PARENT = "_parent";
            public static final String TEXT = "_text";
        }
    }
}
