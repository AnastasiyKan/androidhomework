package com.hfad.bestbeautyassistant.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.hfad.bestbeautyassistant.toolsAndConstants.DbConstant;

/**
 * Created by McLaine on 22.05.2017.
 */

public class Content extends BaseEntity implements Parcelable{
    private int m_type;
    private long m_nParent;
    private String m_strName;
    private String m_strIconName;

    public Content (Cursor cursor) {
        this.m_type = cursor.getInt(cursor.getColumnIndex(DbConstant.TableContent.Fields.TYPE));
        this.m_nParent = cursor.getLong(cursor.getColumnIndex(DbConstant.TableContent.Fields.PARENT));
        this.m_strName = cursor.getString(cursor.getColumnIndex(DbConstant.TableCategories.Fields.NAME));
        this.m_strIconName = cursor.getString(cursor.getColumnIndex(DbConstant.TableCategories.Fields.ICON_NAME));

    }

    public Content(int m_type, long m_nParent, String m_strName, String m_strIconName) {
        this.m_type = m_type;
        this.m_nParent = m_nParent;
        this.m_strName = m_strName;
        this.m_strIconName = m_strIconName;
    }

    protected Content(Parcel in) {
        setId(in.readLong());
        m_type = in.readInt();
        m_nParent = in.readLong();
        m_strName = in.readString();
        m_strIconName = in.readString();
    }

    public static final Creator<Content> CREATOR = new Creator<Content>() {
        @Override
        public Content createFromParcel(Parcel in) {
            return new Content(in);
        }

        @Override
        public Content[] newArray(int size) {
            return new Content[size];
        }
    };

    public int getType() {
        return m_type;
    }

    public void setType(int m_type) {
        this.m_type = m_type;
    }

    public long getParent() {
        return m_nParent;
    }

    public void setParent(long m_nParent) {
        this.m_nParent = m_nParent;
    }

    public String getName() {
        return m_strName;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public String getIconName() {
        return m_strIconName;
    }

    public void setIconName(String m_strIconName) {
        this.m_strIconName = m_strIconName;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();//данные для базы данных
        values.put(DbConstant.TableContent.Fields.TYPE, getType());
        values.put(DbConstant.TableContent.Fields.PARENT, getParent());
        values.put(DbConstant.TableContent.Fields.NAME, getName());
        values.put(DbConstant.TableContent.Fields.ICON_NAME, getIconName());

        return values;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeInt(m_type);
        dest.writeLong(m_nParent);
        dest.writeString(m_strName);
        dest.writeString(m_strIconName);
    }

//        ContentValues values = new ContentValues();//данные для базы данных
//        values = new ContentValues();

//        values.put(DbConstant.TableContent.Fields.TYPE, TYPE_LIST);
////      values.put(DbConstant.TableContent.Fields.PARENT, (-1*nIdMenuOrder2));
//        values.put(DbConstant.TableContent.Fields.NAME, "Прически");
//        values.put(DbConstant.TableContent.Fields.ICON_NAME, "activity_hair_hairstyles");
//        long nIdHairs = db.insert(DbConstant.TableContent.TABLE_NAME, null, values);
//
//        values = new ContentValues();
//        values.put(DbConstant.TableContent.Fields.TYPE, TYPE_FINISHED_ARTICLE);
//        //  values.put(DbConstant.TableContent.Fields.PARENT, );
//        values.put(DbConstant.TableContent.Fields.NAME, "Типы волос");
//        values.put(DbConstant.TableContent.Fields.ICON_NAME, "activity_hair_types_hair");
//        db.insert(DbConstant.TableContent.TABLE_NAME, null, values);
//
//        values = new ContentValues();
//        values.put(DbConstant.TableContent.Fields.TYPE, TYPE_LIST);
//        //  values.put(DbConstant.TableContent.Fields.PARENT, );
//        values.put(DbConstant.TableContent.Fields.NAME, "Уход за волосами в домашних условиях");
//        values.put(DbConstant.TableContent.Fields.ICON_NAME, "activity_hair_hair_care");
//        db.insert(DbConstant.TableContent.TABLE_NAME, null, values);
//
//        values = new ContentValues();
//        values.put(DbConstant.TableContent.Fields.TYPE, TYPE_LIST);
//        //  values.put(DbConstant.TableContent.Fields.PARENT, );
//        values.put(DbConstant.TableContent.Fields.NAME, "Маски для волос в домашних условиях");
//        values.put(DbConstant.TableContent.Fields.ICON_NAME, "activity_hair_hair_masks");
//        db.insert(DbConstant.TableContent.TABLE_NAME, null, values);
//
//        values = new ContentValues();
//        values.put(DbConstant.TableContent.Fields.TYPE, TYPE_FINISHED_ARTICLE);
//        //  values.put(DbConstant.TableContent.Fields.PARENT, );
//        values.put(DbConstant.TableContent.Fields.NAME, "Рецепты для экспресс-восстановления волос");
//        values.put(DbConstant.TableContent.Fields.ICON_NAME, "activity_hair_recipes_for_hair_restoration");
//        db.insert(DbConstant.TableContent.TABLE_NAME, null, values);
//        values = new ContentValues();
//
//        values.put(DbConstant.TableContent.Fields.TYPE, TYPE_FINISHED_ARTICLE);
//        //  values.put(DbConstant.TableContent.Fields.PARENT, );
//        values.put(DbConstant.TableContent.Fields.NAME, "Трендовые окрашивания");
//        values.put(DbConstant.TableContent.Fields.ICON_NAME, "activity_hair_trend_coloring");
//        db.insert(DbConstant.TableContent.TABLE_NAME, null, values);
//
//        values.put(DbConstant.TableContent.Fields.TYPE, TYPE_FINISHED_ARTICLE);
//        //  values.put(DbConstant.TableContent.Fields.PARENT, );
//        values.put(DbConstant.TableContent.Fields.NAME, "Стильный пучок в офис и на вечеринку");
//        values.put(DbConstant.TableContent.Fields.ICON_NAME, "activity_hair_hairstyles_stylish_bundle");
//        db.insert(DbConstant.TableContent.TABLE_NAME, null, values);
//
//        values.put(DbConstant.TableContent.Fields.TYPE, TYPE_FINISHED_ARTICLE);
//        //  values.put(DbConstant.TableContent.Fields.PARENT, );
//        values.put(DbConstant.TableContent.Fields.NAME, "Легкая прическа с двумя косами на каждый день");
//        values.put(DbConstant.TableContent.Fields.ICON_NAME, "activity_hair_hairstyles_stylish_with_two_braids");
//        db.insert(DbConstant.TableContent.TABLE_NAME, null, values);
//
//        values.put(DbConstant.TableContent.Fields.TYPE, TYPE_FINISHED_ARTICLE);
//        //  values.put(DbConstant.TableContent.Fields.PARENT, );
//        values.put(DbConstant.TableContent.Fields.NAME, "Объемная укладка с лентой в стиле Брижит Бардо");
//        values.put(DbConstant.TableContent.Fields.ICON_NAME, "activity_hair_hairstyles_volumetric_packing_with_ribbon_image1");
//        db.insert(DbConstant.TableContent.TABLE_NAME, null, values);
//
//        values.put(DbConstant.TableContent.Fields.TYPE, TYPE_FINISHED_ARTICLE);
//        //  values.put(DbConstant.TableContent.Fields.PARENT, );
//        values.put(DbConstant.TableContent.Fields.NAME, "Простоая прическа с пучком и косами");
//        values.put(DbConstant.TableContent.Fields.ICON_NAME, "activity_hair_hairstyles_bundle_and_scythe");
//        db.insert(DbConstant.TableContent.TABLE_NAME, null, values);
//
//        values.put(DbConstant.TableContent.Fields.TYPE, TYPE_FINISHED_ARTICLE);
//        //  values.put(DbConstant.TableContent.Fields.PARENT, );
//        values.put(DbConstant.TableContent.Fields.NAME, "Корона из кос - простая прическа на основе плетения колосок");
//        values.put(DbConstant.TableContent.Fields.ICON_NAME, "activity_hair_hairstyles_crown_of_braids_image");
//        db.insert(DbConstant.TableContent.TABLE_NAME, null, values);
//
//        values.put(DbConstant.TableContent.Fields.TYPE, TYPE_FINISHED_ARTICLE);
//        //  values.put(DbConstant.TableContent.Fields.PARENT, );
//        values.put(DbConstant.TableContent.Fields.NAME, "Французская коса наискосок и пучок");
//        values.put(DbConstant.TableContent.Fields.ICON_NAME, "activity_hair_hairstyles_french_braid_obliquely_and_bun_image");
//
//        values.put(DbConstant.TableContent.Fields.TYPE, TYPE_FINISHED_ARTICLE);
//        values.put(DbConstant.TableContent.Fields.PARENT, );
//        values.put(DbConstant.TableContent.Fields.NAME, "Быстрый способ сделать кудри");
//        values.put(DbConstant.TableContent.Fields.ICON_NAME, "activity_hair_hairstyles_quick_way_to_make_curls_image");


}
