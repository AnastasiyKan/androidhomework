package com.hfad.bestbeautyassistant.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.hfad.bestbeautyassistant.toolsAndConstants.DbConstant;

import java.security.acl.Permission;

/**
 * Created by McLaine on 22.05.2017.
 */

public class Articles extends BaseEntity implements Parcelable{
    private long m_nParent;
    private String m_strText;

    public Articles (Cursor cursor){
        this.m_nParent = cursor.getLong(cursor.getColumnIndex(DbConstant.TableArticles.Fields.PARENT));
        this.m_strText = cursor.getString(cursor.getColumnIndex(DbConstant.TableArticles.Fields.TEXT));
    }

    public Articles(long m_nParent, String m_strText) {
        this.m_nParent = m_nParent;
        this.m_strText = m_strText;
    }

    protected Articles(Parcel in) {
        setId(in.readLong());
        m_nParent = in.readLong();
        m_strText = in.readString();
    }

    public static final Creator<Articles> CREATOR = new Creator<Articles>() {
        @Override
        public Articles createFromParcel(Parcel in) {
            return new Articles(in);
        }

        @Override
        public Articles[] newArray(int size) {
            return new Articles[size];
        }
    };

    public String getText() {
        return m_strText;
    }

    public void setText(String m_strText) {
        this.m_strText = m_strText;
    }

    public long getParent() {
        return m_nParent;
    }

    public void setParent(long m_nParent) {
        this.m_nParent = m_nParent;
    }

    @Override
    public ContentValues getContentValues() {

        ContentValues values = new ContentValues();//данные для базы данны[
        values.put(DbConstant.TableContent.Fields.PARENT, getParent());
        values.put(DbConstant.TableArticles.Fields.TEXT, getText());
        return values;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeLong(m_nParent);
        dest.writeString(m_strText);
    }


//        ContentValues values = new ContentValues();//данные для базы данных
//        values = new ContentValues();
//        //  values.put(DbConstant.TableContent.Fields.PARENT, );
//        values.put(DbConstant.TableArticles.Fields.TEXT, "");
//        db.insert(DbConstant.TableContent.TABLE_NAME, null, values);

}
