package com.hfad.bestbeautyassistant.activities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.hfad.bestbeautyassistant.R;

public class HairHairstylesFrenchBraidObliquelyAndBunImageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hair_hairstyles_french_braid_obliquely_and_bun);
    }
}
