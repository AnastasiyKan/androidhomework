package com.hfad.bestbeautyassistant.model;

import android.content.ContentValues;
import android.database.Cursor;
import android.os.Parcel;
import android.os.Parcelable;

import com.hfad.bestbeautyassistant.toolsAndConstants.DbConstant;

/**
 * Created by McLaine on 22.05.2017.
 */

public class Categories extends BaseEntity implements Parcelable{

    private int m_nOrder;
    private  String m_strName;
    private String m_strIconName;

    public Categories (Cursor cursor){
        this.m_nOrder = cursor.getInt(cursor.getColumnIndex(DbConstant.TableCategories.Fields.ORDER));
        this.m_strName = cursor.getString(cursor.getColumnIndex(DbConstant.TableCategories.Fields.NAME));
        this.m_strIconName = cursor.getString(cursor.getColumnIndex(DbConstant.TableCategories.Fields.ICON_NAME));
    }

    public Categories(int m_nOrder, String m_strName, String m_strIconName) {
        this.m_nOrder = m_nOrder;
        this.m_strName = m_strName;
        this.m_strIconName = m_strIconName;
    }

    protected Categories(Parcel in) {
        setId(in.readLong());
        m_nOrder = in.readInt();
        m_strName = in.readString();
        m_strIconName = in.readString();
    }

    public static final Creator<Categories> CREATOR = new Creator<Categories>() {
        @Override
        public Categories createFromParcel(Parcel in) {
            return new Categories(in);
        }

        @Override
        public Categories[] newArray(int size) {
            return new Categories[size];
        }
    };

    public int getOrder() {
        return m_nOrder;
    }

    public void setOrder(int m_order) {
        this.m_nOrder = m_order;
    }

    public String getName() {
        return m_strName;
    }

    public void setName(String m_strName) {
        this.m_strName = m_strName;
    }

    public String getIconName() {
        return m_strIconName;
    }

    public void setIconName(String m_strIconName) {
        this.m_strIconName = m_strIconName;
    }

    @Override
    public ContentValues getContentValues() {
        ContentValues values = new ContentValues();//данные для базы данных
        values.put(DbConstant.TableCategories.Fields.ORDER, getOrder());
        values.put(DbConstant.TableCategories.Fields.NAME, getName());
        values.put(DbConstant.TableCategories.Fields.ICON_NAME, getIconName());
        return values;
    }

    @Override
    public int describeContents() {

        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeLong(getId());
        dest.writeInt(m_nOrder);
        dest.writeString(m_strName);
        dest.writeString(m_strIconName);
    }


//
//        ContentValues values = new ContentValues();//данные для базы данных
//        values.put(DbConstant.TableCategories.Fields.ORDER, 1);
//        values.put(DbConstant.TableCategories.Fields.NAME, "Уход за лицом");
//        values.put(DbConstant.TableCategories.Fields.ICON_NAME, "activity_main_face");
////        db.insert(DbConstant.TableCategories.TABLE_NAME, null, values);
//
//        values = new ContentValues();//данные для базы данных
//        values.put(DbConstant.TableCategories.Fields.ORDER, 2);
//        values.put(DbConstant.TableCategories.Fields.NAME, "Уход за волосами");
//        values.put(DbConstant.TableCategories.Fields.ICON_NAME, "activity_main_hair");
////        long nIdMenuOrder2 = db.insert(DbConstant.TableCategories.TABLE_NAME, null, values);
//
//        values = new ContentValues();
//        values.put(DbConstant.TableCategories.Fields.ORDER, 3);
//        values.put(DbConstant.TableCategories.Fields.NAME, "Уход за руками и ногами");
//        values.put(DbConstant.TableCategories.Fields.ICON_NAME, "activity_main_hand_feet");
////        db.insert(DbConstant.TableCategories.TABLE_NAME, null, values);
//
//        values = new ContentValues();
//        values.put(DbConstant.TableCategories.Fields.ORDER, 4);
//        values.put(DbConstant.TableCategories.Fields.NAME, "Правильное питание");
//        values.put(DbConstant.TableCategories.Fields.ICON_NAME, "activity_main_proper_nutrition");
////        db.insert(DbConstant.TableCategories.TABLE_NAME, null, values);
//
//        values = new ContentValues();
//        values.put(DbConstant.TableCategories.Fields.ORDER, 5);
//        values.put(DbConstant.TableCategories.Fields.NAME, "Фитнес");
//        values.put(DbConstant.TableCategories.Fields.ICON_NAME, "activity_main_fitness");
////        db.insert(DbConstant.TableCategories.TABLE_NAME, null, values);
//
//        values = new ContentValues();
//        values.put(DbConstant.TableCategories.Fields.ORDER, 6);
//        values.put(DbConstant.TableCategories.Fields.NAME, "Магазины на карте");
//        values.put(DbConstant.TableCategories.Fields.ICON_NAME, "activity_main_shop");
////        db.insert(DbConstant.TableCategories.TABLE_NAME, null, values);

}
