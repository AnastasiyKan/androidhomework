package com.hfad.bestbeautyassistant.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

import com.hfad.bestbeautyassistant.R;

public class HairHairstylesActivity extends AppCompatActivity implements View.OnClickListener {
    View activity_hair_hairstyles_stylish_bundle, activity_hair_hairstyles_stylish_with_two_braids, activity_hair_hairstyles_volumetric_packing_with_ribbon;
    View activity_hair_hairstyles_bundle_and_scythe, activity_hair_hairstyles_crown_of_braids, activity_hair_hairstyles_french_braid_obliquely_and_bun;
    View activity_hair_hairstyles_quick_way_to_make_curls;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hair_hairstyles);

//        ActionBar actionBar = getSupportActionBar();
//        assert actionBar != null;
//        actionBar.setHomeButtonEnabled(true);
//        actionBar.setDisplayHomeAsUpEnabled(true);

        activity_hair_hairstyles_stylish_bundle = findViewById(R.id.stylish_bundle);
        activity_hair_hairstyles_stylish_bundle.setOnClickListener(this);
        activity_hair_hairstyles_stylish_with_two_braids = findViewById(R.id.activity_hair_hairstyles_stylish_with_two_braids);
        activity_hair_hairstyles_stylish_with_two_braids.setOnClickListener(this);
        activity_hair_hairstyles_volumetric_packing_with_ribbon = findViewById(R.id.activity_hair_hairstyles_volumetric_packing_with_ribbon);
        activity_hair_hairstyles_volumetric_packing_with_ribbon.setOnClickListener(this);
        activity_hair_hairstyles_bundle_and_scythe = findViewById(R.id.activity_hair_hairstyles_bundle_and_scythe);
        activity_hair_hairstyles_bundle_and_scythe.setOnClickListener(this);
        activity_hair_hairstyles_crown_of_braids = findViewById(R.id.activity_hair_hairstyles_crown_of_braids);
        activity_hair_hairstyles_crown_of_braids.setOnClickListener(this);
        activity_hair_hairstyles_french_braid_obliquely_and_bun = findViewById(R.id.activity_hair_hairstyles_french_braid_obliquely_and_bun);
        activity_hair_hairstyles_french_braid_obliquely_and_bun.setOnClickListener(this);
        activity_hair_hairstyles_quick_way_to_make_curls = findViewById(R.id.activity_hair_hairstyles_quick_way_to_make_curls);
        activity_hair_hairstyles_quick_way_to_make_curls.setOnClickListener(this);

    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {// обрабатываем нажатие на кнопку Home
//        switch (item.getItemId()) {
//            case android.R.id.home:
//                startActivity(new Intent(this, ListActivity.class));
//                return true;
//            default:
//                return super.onOptionsItemSelected(item);
//
//        }
//    }
//
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {//добавляем избранные и менюшку
//        getMenuInflater().inflate(R.menu.menu_main, menu);
//        return true;
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.stylish_bundle:
                Intent intent = new Intent(this, HairHairstylesStylishBundleActivity.class);
                startActivity(intent);
                break;
            case R.id.activity_hair_hairstyles_stylish_with_two_braids:
                Intent intent1 = new Intent(this, HairHairstylesStylishWithTwoBraidsActivity.class);
                startActivity(intent1);
                break;
            case R.id.activity_hair_hairstyles_volumetric_packing_with_ribbon:
                Intent intent2 = new Intent(this, HairHairstylesVolumetricPackingWithRibbonActivity.class);
                startActivity(intent2);
                break;
            case R.id.activity_hair_hairstyles_bundle_and_scythe:
                Intent intent3 = new Intent(this, HairHairstylesBundleAndScytheActivity.class);
                startActivity(intent3);
                break;
            case R.id.activity_hair_hairstyles_crown_of_braids:
                Intent intent4 = new Intent(this, HairHairstylesCrownOfBraidsActivity.class);
                startActivity(intent4);
                break;
            case R.id.activity_hair_hairstyles_french_braid_obliquely_and_bun:
                Intent intent5 = new Intent(this, HairHairstylesFrenchBraidObliquelyAndBunImageActivity.class);
                startActivity(intent5);
                break;
            case R.id.activity_hair_hairstyles_quick_way_to_make_curls:
                Intent intent6 = new Intent(this, HairHairstylesQuickWayToMakeCurlsActivity.class);
                startActivity(intent6);
                break;



        }
    }
}
