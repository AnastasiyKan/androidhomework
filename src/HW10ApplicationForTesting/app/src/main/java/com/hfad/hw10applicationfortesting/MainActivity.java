package com.hfad.hw10applicationfortesting;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    Button buttonFurther;
    EditText editTextName;
    public final static String TEG = "kan";
    public final static String PUT_NAME = "PUT_NAME";
    public final static String COLOR = "COLOR";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        buttonFurther = (Button) findViewById(R.id.buttonFurther);
        buttonFurther.setOnClickListener(this);
        editTextName = (EditText) findViewById(R.id.editTextName);

        ImageView imageView = (ImageView) findViewById(R.id.image_layout1);
        imageView.setImageResource(R.drawable.rose);

        Log.d(TEG, "MainActivity -> onCreate "+R.layout.activity_main);
    }

    @Override
    protected void onStart() {
        super.onStart();
        Log.d(TEG, "MainActivity -> onStart ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(TEG, "MainActivity -> onStop ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(TEG, "MainActivity -> onResum ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(TEG, "MainActivity -> onPause ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(TEG, "MainActivity -> onRestart ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TEG, "MainActivity -> onDestroy ");
    }


    @Override
    public void onClick(View v) {
        Log.d(TEG, "MainActivity -> onClick "+v.getId());
        switch (v.getId()){
            case R.id.buttonFurther:
                String name = editTextName.getText().toString();
                if ("".equals(name)){
                    Toast.makeText(this, "Write your name", Toast.LENGTH_LONG).show();
                    return;
                }
                Intent intent = new Intent(this, TwoActivity.class);
                intent.putExtra(PUT_NAME,name);
                startActivity(intent);
                break;
        }

       }
}
