package com.hfad.hw10applicationfortesting;

import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by McLaine on 29.03.2017.
 */

public class ThreeActivity extends AppCompatActivity {
    TextView textPersonCaracter;
    String character;
    Drawable imageView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_three);
        Log.d(MainActivity.TEG, "ThreeActivity -> onCreate "+R.layout.activity_main);
        textPersonCaracter = (TextView) findViewById(R.id.textViewLayoutThree);
        Intent intent = getIntent();
        if (intent!=null){
            Bundle bundle = intent.getExtras();
            if (bundle!=null){
                character = bundle.getString(MainActivity.COLOR, " ");
            }
        }
        textPersonCaracter.setText(character);


        }



    @Override
    protected void onStart() {
        super.onStart();
        Log.d(MainActivity.TEG, "ThreeActivity -> onStart ");
    }

    @Override
    protected void onStop() {
        super.onStop();
        Log.d(MainActivity.TEG, "ThreeActivity -> onStop ");
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.d(MainActivity.TEG, "ThreeActivity -> onResum ");
    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.d(MainActivity.TEG, "ThreeActivity -> onPause ");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        Log.d(MainActivity.TEG, "ThreeActivity -> onRestart ");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(MainActivity.TEG, "ThreeActivity -> onDestroy ");
    }
}
