package com.hfad.hw9calculator;

import android.app.Activity;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements View.OnClickListener {
    Button b1, b2, b3, b4, b5, b6, b7, b8, b9, b0, b_erase, b_equally, b_plus, b_minus, b_divide, b_miltipl;
    TextView textView;
    int number1, number2, action;
    double result;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textView = (TextView) findViewById(R.id.texеAnswer);
        number1 = 0;
        number2 = 0;
        action = 0;
        result = 0;

        b1 = (Button) findViewById(R.id.button1);
        b2 = (Button) findViewById(R.id.button2);
        b3 = (Button) findViewById(R.id.button3);
        b4 = (Button) findViewById(R.id.button4);
        b5 = (Button) findViewById(R.id.button5);
        b6 = (Button) findViewById(R.id.button6);
        b7 = (Button) findViewById(R.id.button7);
        b8 = (Button) findViewById(R.id.button8);
        b9 = (Button) findViewById(R.id.button9);
        b0 = (Button) findViewById(R.id.button0);
        b_erase = (Button) findViewById(R.id.button_erase);
        b_equally = (Button) findViewById(R.id.button_equally);
        b_plus = (Button) findViewById(R.id.button_plus);
        b_minus = (Button) findViewById(R.id.button_minus);
        b_divide = (Button) findViewById(R.id.button_divide);
        b_miltipl = (Button) findViewById(R.id.button_miltipl);

        b1.setOnClickListener(this);
        b2.setOnClickListener(this);
        b3.setOnClickListener(this);
        b4.setOnClickListener(this);
        b5.setOnClickListener(this);
        b6.setOnClickListener(this);
        b7.setOnClickListener(this);
        b8.setOnClickListener(this);
        b9.setOnClickListener(this);
        b0.setOnClickListener(this);
        b_divide.setOnClickListener(this);
        b_equally.setOnClickListener(this);
        b_erase.setOnClickListener(this);
        b_miltipl.setOnClickListener(this);
        b_minus.setOnClickListener(this);
        b_plus.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button1:
                ClickNumber(1);
                break;
            case R.id.button2:
                ClickNumber(2);
                break;
            case R.id.button3:
                ClickNumber(3);
                break;
            case R.id.button4:
                ClickNumber(4);
                break;
            case R.id.button5:
                ClickNumber(5);
                break;
            case R.id.button6:
                ClickNumber(6);
                break;
            case R.id.button7:
                ClickNumber(7);
                break;
            case R.id.button8:
                ClickNumber(8);
                break;
            case R.id.button9:
                ClickNumber(9);
                break;
            case R.id.button0:
                ClickNumber(0);
                break;
            case R.id.button_plus:
                if (action == 0) action = 1;
                break;
            case R.id.button_minus:
                if (action == 0) action = 2;
                break;
            case R.id.button_miltipl:
                if (action == 0) action = 3;
                break;
            case R.id.button_divide:
                if (action == 0) action = 4;
                break;
            case R.id.button_equally:
                switch (action) {
                    case 1:
                        result = number1 + number2;
                        break;
                    case 2:
                        result = number1 - number2;
                        break;
                    case 3:
                        result = number1 * number2;
                        break;
                    case 4:
                        result = number1 / number2;
                        break;
                }
                if (action != 0) {
                    textView.setText(Double.toString(result));
                    number1 = 0;
                    number2 = 0;
                    action = 0;
                    result = 0;

                }
                break;
            case R.id.button_erase:
                number1 = 0;
                number2 = 0;
                action = 0;
                result = 0;
                textView.setText(Double.toString(number1));
                break;
        }
    }

    private void ClickNumber(int i) {
        if (action == 0) {
            number1 = number1 * 10 + i;
            textView.setText(Integer.toString(number1));
        } else {
            number2 = number2 * 10 + i;
            textView.setText(Integer.toString(number2));

        }

    }
}

