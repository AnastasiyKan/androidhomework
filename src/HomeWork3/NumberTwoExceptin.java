package HomeWork3;

/**
 * Created by McLaine on 13.03.2017.
 */
public class NumberTwoExceptin extends Exception{

    public NumberTwoExceptin() {
    }

    public NumberTwoExceptin(String message) {
        super(message);
    }

    public NumberTwoExceptin(String message, Throwable cause) {
        super(message, cause);
    }

    public NumberTwoExceptin(Throwable cause) {
        super(cause);
    }

    public NumberTwoExceptin(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}


