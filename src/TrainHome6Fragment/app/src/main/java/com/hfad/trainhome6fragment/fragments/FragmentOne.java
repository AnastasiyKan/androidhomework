package com.hfad.trainhome6fragment.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.hfad.trainhome6fragment.R;
import com.hfad.trainhome6fragment.activities.MainActivity;

/**
 * Created by McLaine on 30.03.2017.
 */

public class FragmentOne extends Fragment implements View.OnClickListener{
    Button b1, b2;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_one, container, false);
        b1 = (Button) view.findViewById(R.id.buttonFragment1_1);
        b2 = (Button) view.findViewById(R.id.buttonFragment1_2);
        b1.setOnClickListener(this);
        b2.setOnClickListener(this);

        return view;
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.buttonFragment1_1:
                Activity activity = getActivity();
                if (activity instanceof MainActivity){
                    MainActivity mainActivity = (MainActivity)activity;
                    mainActivity.deleteFragment1();
                }
                break;
            case R.id.buttonFragment1_2:
                Activity activity1 = getActivity();
                if (activity1 instanceof MainActivity){
                    MainActivity mainActivity = (MainActivity)activity1;
                    mainActivity.deleteFragment2();
                }

        }


    }
}
