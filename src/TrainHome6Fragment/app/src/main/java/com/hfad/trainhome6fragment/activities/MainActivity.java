package com.hfad.trainhome6fragment.activities;

import android.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.hfad.trainhome6fragment.R;
import com.hfad.trainhome6fragment.fragments.FragmentOne;
import com.hfad.trainhome6fragment.fragments.FragmentTwo;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    FragmentOne fragmentOne;
    FragmentTwo fragmentTwo;
    TextView textView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        fragmentOne = new FragmentOne();
        fragmentTwo = new FragmentTwo();
        textView = (TextView) findViewById(R.id.textLayout1);
        textView.setOnClickListener(this);
        findViewById(R.id.buttonActivity1).setOnClickListener(this);
        findViewById(R.id.buttonActivity2).setOnClickListener(this);
        findViewById(R.id.buttonActivity3).setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        switch (v.getId()) {
            case R.id.buttonActivity1:
                textView.setText(R.string.add_fragment_first);
                if (fragmentTwo.isAdded())fragmentTransaction.remove(fragmentTwo);
                if (fragmentOne.isAdded()) {
                    fragmentTransaction.replace(R.id.frameLayout1, fragmentOne);
                } else {
                    fragmentTransaction.add(R.id.frameLayout1, fragmentOne);
                }
                break;
            case R.id.buttonActivity2:
                textView.setText(R.string.add_fragment_second);
                if (fragmentOne.isAdded())fragmentTransaction.remove(fragmentOne);
                if (fragmentTwo.isAdded()) {
                    fragmentTransaction.replace(R.id.frameLayout2, fragmentTwo);
                } else {
                    fragmentTransaction.add(R.id.frameLayout2, fragmentTwo);
                }
                break;
            case R.id.buttonActivity3:
                textView.setText(R.string.add_both_fragments);
                if (fragmentOne.isAdded()&&fragmentTwo.isAdded()){
                }else if (fragmentOne.isAdded()){
                    fragmentTransaction.replace(R.id.frameLayout1, fragmentOne);
                    fragmentTransaction.add(R.id.frameLayout2, fragmentTwo);
                }else if (fragmentTwo.isAdded()){
                    fragmentTransaction.replace(R.id.frameLayout2, fragmentTwo);
                    fragmentTransaction.add(R.id.frameLayout1, fragmentOne);
                }else {
                    fragmentTransaction.add(R.id.frameLayout1, fragmentOne);
                    fragmentTransaction.add(R.id.frameLayout2, fragmentTwo);
                }

                break;


        }
        fragmentTransaction.commit();
    }
    public void  deleteFragment1(){
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.remove(fragmentOne);
        fragmentTransaction.commit();
    }
    public void  deleteFragment2(){
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.remove(fragmentTwo);
        fragmentTransaction.commit();
    }
}
