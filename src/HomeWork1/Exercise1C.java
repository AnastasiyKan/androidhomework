package HomeWork1;

import java.util.Scanner;

/**
 * Created by Анастасия on 27.02.2017.
 */
public class Exercise1C {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длину массива: ");
        int lengsArray = scanner.nextInt();
        int [] array = new int [lengsArray];

        System.out.println("Заполните массив: ");

        for (int i = 0; i<lengsArray;i++){
            array [i] = scanner.nextInt();
        }
        int multipl = 1;
        int halfOf = lengsArray/2;
        for (int i = 0; i!=array.length;i++){
            if (i<halfOf){
                multipl*=array[i];
            }
        }
        System.out.println(multipl);

    }
}

