package HomeWork1;

import java.util.Scanner;

/**
 * Created by McLaine on 02.03.2017.
 */
public class Exercise1E {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длину массива: ");
        int lenghtArray = scanner.nextInt();
        int [] array = new int[lenghtArray];

        System.out.println("Заполните массив: ");
        for (int i = 0;i<lenghtArray;i++){
            array [i] = scanner.nextInt();
        }

        int summ = 0;
        for (int i=0;i<array.length;i++){
            if (i%2!=0){
                summ+=array[i];
            }
        }
        System.out.println("Сумма всех элеметов с нечетным индексом: "+summ);
    }
}
