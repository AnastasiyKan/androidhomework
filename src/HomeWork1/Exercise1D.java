package HomeWork1;

import java.util.Scanner;

/**
 * Created by McLaine on 02.03.2017.
 */
public class Exercise1D {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);


        System.out.print("Введите длину массива: ");
        int lengthArray = scanner.nextInt();
        int [] array = new int [lengthArray];

        System.out.println("Заполните массив:");
        for (int i = 0;i<lengthArray;i++) {
            array[i] = scanner.nextInt();
        }
        int min = array [0];
        for (int i=0;i<array.length;i++){
            if (array[i]<min){
                min = array [i];
            }
        }
        System.out.println("Минимальное число в массиве: "+min);
    }
}
