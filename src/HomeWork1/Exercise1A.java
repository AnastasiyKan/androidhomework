package HomeWork1;

import java.util.Scanner;

        public class Exercise1A {

            public static void main(String[] args) {
                Scanner scanner = new Scanner(System.in);
                System.out.println("Введите длину массива: ");
                int lengthArray = scanner.nextInt();
                int [] array = new int[lengthArray];

                System.out.println("Заполните массив: ");
                for (int i = 0;i<lengthArray;i++) {
                    array [i] = scanner.nextInt();
                }
                System.out.print("Добавленные элементы массива: ");
                for (int i = 0;i<lengthArray;i++) {
                    System.out.print(" "+array[i]);
                }
                System.out.println();
                int max = array [0];

                for(int i = 0; i != array.length; i ++){
                    if(array[i] > max){
                        max = array[i];
                    }
                }
                System.out.println("Максимальное число в массиве: " + max);
            }

        }

