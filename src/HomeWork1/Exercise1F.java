package HomeWork1;

import java.util.Scanner;

/**
 * Created by McLaine on 02.03.2017.
 */
public class Exercise1F {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длину массива: ");
        int lenghtArray = scanner.nextInt();
        int [] array = new int[lenghtArray];


        System.out.println("Заполните массив: ");
        for (int i = 0; i<lenghtArray;i++){
            array [i] = scanner.nextInt();
        }
        int multipl = 1;
        int halfOf = lenghtArray/2;
        for (int i = 0; i!=array.length;i++) {
            if (i >= halfOf) {
                multipl *= array[i];
            }
        }
        System.out.println("Произведение всех чисел из второй половины массива: "+multipl);
    }
}
