package HomeWork1.Exercise4;

/**
 * Created by McLaine on 06.03.2017.
 */
public class SorterMinus {
    private int [] mas;
    public SorterMinus (int [] mas){
        this.mas = mas;
    }
    private void swap(int[] mas, int index) {
        int temp = mas[index-1];
        mas[index-1]=mas[index];
        mas[index]=temp;
    }
    public int [] bibleSort(){
        int [] mas2 = mas;

        for (int i = 1; i<mas2.length;i++){
            for (int j = mas2.length-1;j>=i;j--){
                if(mas2[j-1]<mas2[j]){
                    swap(mas2, j);
                }
            }
        }
        return  mas2;
    }
}
