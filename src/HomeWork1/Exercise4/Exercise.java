package HomeWork1.Exercise4;

import java.util.Scanner;

/**
 * Created by McLaine on 04.03.2017.
 */
public class Exercise {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int [] mas = new int [10];

        System.out.println("Заполните массив: ");
        for (int i = 0; i<mas.length;i++){
            mas[i]=scanner.nextInt();
        }
        System.out.println("");
        Sorter sorter = new Sorter(mas);
        mas = sorter.bubbleSort();

        System.out.print("Добавленные элементы массива в порядке возрастание: ");
        for (int i = 0; i<mas.length;i++){
            System.out.print(mas[i]+" ");
        }
        System.out.println("");

        SorterMinus sorterMinus = new SorterMinus(mas);
        mas = sorterMinus.bibleSort();
        System.out.print("Добавленные элементы массива в порядке убываниия: ");
        for (int i = 0; i<mas.length;i++){
            System.out.print(mas[i]+" ");
        }
        System.out.println("");

    }

}

