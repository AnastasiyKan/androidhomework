package HomeWork1.Exercise4;

/**
 * Created by McLaine on 06.03.2017.
 */

    public class Sorter{
        private int [] mas;
        public Sorter(int[]mas){

            this.mas = mas;
        }

        private void swap(int[] mas, int index) {
            int temp =   mas[index-1];
            mas[index-1]=mas[index];
            mas[index]=temp;
        }
        public int[] bubbleSort(){
            int [] mas2 = mas;
            for (int i = 1; i < mas2.length; i++){
                for (int j = mas2.length-1; j>=i; j--){
                    if (mas2[j-1]>mas2[j]){
                        swap(mas2, j);
                    }
                }
            }
            return mas2;
        }

}

