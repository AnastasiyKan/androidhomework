package HomeWork1;

import java.util.Arrays;
import java.util.Scanner;

/**
 * Created by McLaine on 02.03.2017.
 */
public class Exercise2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите строку: ");
        String s = scanner.nextLine();

        System.out.println(Arrays.toString(s.split(" ")));

    }
}
