package HomeWork1;

import java.util.Scanner;

/**
 * Created by Анастасия on 27.02.2017.
 */
public class Exercise1B {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Введите длину массива: ");
        int lengthArray = scanner.nextInt();
        int [] array = new int[lengthArray];

        System.out.println("Заполните массив: ");
        for (int i = 0;i<lengthArray;i++) {
            array [i] = scanner.nextInt();
        }
        System.out.print("Добавленные элементы массива: ");
        for (int i = 0;i<lengthArray;i++) {
            System.out.print(" "+array[i]);
        }
        System.out.println();
        int summ = 0;
        for (int i = 0; i!=array.length;i++){
            if (i%2==0){
                summ+=array[i];

            }
        }
        System.out.println("Сумма всех элеметов с четным индексом: "+summ);
    }

}
